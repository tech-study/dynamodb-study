package com.toyfactory.test.dynamodb.singletable;

import com.toyfactory.test.dynamodb.singletable.domain.BaseKey;
import com.toyfactory.test.dynamodb.singletable.domain.Game;
import com.toyfactory.test.dynamodb.singletable.domain.User2;
import com.toyfactory.test.dynamodb.repository.GameRepository;
import com.toyfactory.test.dynamodb.repository.User2Repository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ezkiro on 2021-05-26
 */

@Service
@RequiredArgsConstructor
public class SingleService {

    private final User2Repository user2Repository;
    private final GameRepository gameRepository;

    public void createUser(String userName, String email) {
        User2 newUser = new User2(userName, email);

        user2Repository.save(newUser);
    }

    public User2 findUser(String userName) throws Exception {

        return user2Repository.findByKey(User2.makeKey(userName)).orElseThrow(() -> new Exception("Not found! userName:" + userName));
    }

    public List<User2> findAllUser() {
        return user2Repository.findByKeyContaining(User2.keyPrefix());
    }

    public void createGame(String gameId, String desc) {

        Game newGame = new Game(gameId, desc);

        gameRepository.save(newGame);
    }

    public Game findGame(String gameId) throws Exception {

        BaseKey key = new BaseKey(Game.makeKey(gameId), Game.makeSortKey(gameId));

        return gameRepository.findById(key).orElseThrow(() -> new Exception("not found! gameId:" + gameId));
    }
}
