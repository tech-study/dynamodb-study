package com.toyfactory.test.dynamodb.singletable.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import java.io.Serializable;

/**
 * Created by ezkiro on 2021-05-27
 * RangeKey 를 사용하려면 composite key 형태로 사용을 해야 한다.
 */

public class BaseKey implements Serializable {
    private static final long serialVersionUID = 2390504448877689618L;

    private String key;
    private String sortKey;

    @DynamoDBHashKey
    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @DynamoDBRangeKey
    public String getSortKey() {
        return this.sortKey;
    }

    public void setSortKey(String key) {
        this.sortKey = key;
    }

    protected BaseKey() {}

    public BaseKey(String key, String sortKey) {
        this.key = key;
        this.sortKey = sortKey;
    }
}
