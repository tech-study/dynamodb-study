package com.toyfactory.test.dynamodb.singletable;

import com.toyfactory.test.dynamodb.singletable.domain.Game;
import com.toyfactory.test.dynamodb.singletable.domain.User2;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by ezkiro on 2021-05-26
 */

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping(value = "/single")
public class SingleController {

    private final SingleService singleService;

    @PostMapping(value = "/create/user/{userName}")
    public String createUser(@PathVariable String userName) {
        singleService.createUser(userName, userName + "@test.com");
        return "OK";
    }

    @PostMapping(value = "/create/game/{gameId}")
    public String createGame(@PathVariable String gameId) {
        singleService.createGame(gameId, "this is sample");
        return "OK";
    }


    @GetMapping(value = "/find/user/{userName}")
    public User2 findUser(@PathVariable String userName) throws Exception {
        return singleService.findUser(userName);
    }

    @GetMapping(value = "/find/alluser")
    public List<User2> findAllUser(){
        return singleService.findAllUser();
    }

    @GetMapping(value = "/find/game/{gameid}")
    public Game findGame(@PathVariable String gameid) throws Exception {
        return singleService.findGame(gameid);
    }
}
