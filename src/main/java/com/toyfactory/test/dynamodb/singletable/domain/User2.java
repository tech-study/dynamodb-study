package com.toyfactory.test.dynamodb.singletable.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import org.springframework.data.annotation.Id;

/**
 * Created by ezkiro on 2021-05-26
 */

@DynamoDBTable(tableName = "NPS-Test")
public class User2 {

    @Id
    private BaseKey id;
    private String userName;
    private String email;

    @DynamoDBHashKey(attributeName = "key")
    public String getKey() {
        //USER#<userName>
        return id != null ? id.getKey() : null;
    }

    public void setKey(String key) {
        if (id == null) {
            id = new BaseKey();
        }
        id.setKey(key);
    }

    @DynamoDBRangeKey(attributeName = "sortKey")
    public String getSortKey() {
        //#METADATA#<userName>
        return id != null ? id.getSortKey() : null;
    }

    public void setSortKey(String key) {
        if (id == null) {
            id = new BaseKey();
        }
        id.setSortKey(key);
    }


    public static String makeKey(String userName) {

        return "USER#" + userName;
    }

    public static String makeSortKey(String userName) {

        return "#METADATA#" + userName;
    }

    public static String keyPrefix() {
        return "USER#";
    }

    public User2() {}

    public User2(String userName, String email) {
        this.id = new BaseKey(makeKey(userName),makeSortKey(userName));
        this.userName = userName;
        this.email = email;
    }

    @DynamoDBAttribute
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @DynamoDBAttribute
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
