package com.toyfactory.test.dynamodb.singletable.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import org.springframework.data.annotation.Id;

/**
 * Created by ezkiro on 2021-05-26
 */


@DynamoDBTable(tableName = "NPS-Test")
public class GameUserMapping {

    @Id
    private BaseKey id;

    private String gameId;

    private String userName;

    @DynamoDBHashKey(attributeName = "key")
    public String getKey() {
        //GAME#<GAME_ID>
        return id != null ? id.getKey() : null;
    }

    public void setKey(String key) {
        if (id == null) {
            id = new BaseKey();
        }
        id.setKey(key);
    }

    @DynamoDBRangeKey(attributeName = "sortKey")
    public String getSortKey() {
        //USER#<USER_NAME>
        return id != null ? id.getSortKey() : null;
    }

    public void setSortKey(String key) {
        if (id == null) {
            id = new BaseKey();
        }
        id.setSortKey(key);
    }


    @DynamoDBAttribute
    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @DynamoDBAttribute
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public static String makeKey(String gameId) {

        return "GAME#" + gameId;
    }

    public static String makeSortKey(String userName) {

        return "USER#" + userName;
    }

    public GameUserMapping() {}

    public GameUserMapping(String gameId, String userName) {
        this.id = new BaseKey(makeKey(gameId), makeSortKey(userName));
        this.gameId = gameId;
        this.userName = userName;
    }
}
