package com.toyfactory.test.dynamodb.sample;

import com.toyfactory.test.dynamodb.repository.MemberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class MemberController {

    private static Logger logger = LoggerFactory.getLogger(MemberController.class);

    private final MemberRepository memberRepository;

    private final MemberService memberService;

    public MemberController(MemberRepository memberRepository, MemberService memberService) {
        this.memberRepository = memberRepository;
        this.memberService = memberService;
    }

    @PostMapping(value="/create")
    public User createMember(@RequestParam String firstName,
                             @RequestParam String lastName) {
        return memberService.create(firstName, lastName);
    }

    @PostMapping(value = "/add/job/{id}")
    public Boolean addJobs(@PathVariable String id) {
        List<Job> jobs = new ArrayList<>();

        Job job1 = new Job();
        job1.setName("simple");
        job1.setSalary(123);
        jobs.add(job1);

        Job job2 = new Job();
        job2.setName("hobby");
        job2.setSalary(234);
        jobs.add(job2);

        Job job3 = new Job();
        job3.setName("worker");
        job3.setSalary(999);
        jobs.add(job3);

        jobs.forEach(job -> {
            memberService.addJob(id, job);
        });

        return Boolean.TRUE;
    }

    @GetMapping(value="/user/{firstName}")
    public List<User> findByFirstName(@PathVariable String firstName) {
        return memberRepository.findByFirstName(firstName);
    }

    @GetMapping(value="/user/all")
    public List<User> findByAll() {
        List<User> users = new ArrayList<>();
        memberRepository.findAll().forEach(users::add);
        return users;
    }

    @GetMapping(value="/user/one/{id}")
    public Optional<User> findOne(@PathVariable String id) {
        logger.debug("============ find one:" + id);

        Optional<User> user = memberRepository.findById(id);
        user.ifPresent(user1 -> {
            user1.getMyJobs().forEach(job -> {
                logger.debug("my job:" + job.getName());
            });
        });
        return user;
    }

    @DeleteMapping(value="/user/one/{id}")
    public String deleteOne(@PathVariable String id) {
        memberService.destroy(id);
        return id + " is deleted!!";
    }

    @PostMapping(value = "/user/birthday/{id}")
    public String updateBirthDay(@PathVariable String id) {
        memberService.updateBirthDt(id);
        return id + " birth day is updated!!";
    }

    @GetMapping(value="/test/user")
    public User getUser() {
        User user1 = new User();
        user1.setFirstName("firstName");
        user1.setLastName("lastName");

        Address address = new Address();
        address.setCity("suwon city");
        address.setZipCode(100);
        address.setStreet("wellbing-ro 70");
        user1.setAddress(address);

        Job job1 = new Job();
        job1.setName("simple");
        job1.setSalary(123);

        Job job2 = new Job();
        job2.setName("hobby");
        job2.setSalary(234);

        Job job3 = new Job();
        job3.setName("worker");
        job3.setSalary(999);

        List<Job> jobList = new ArrayList<>();
        Set<Job> myJobs = new LinkedHashSet<>();
        jobList.add(job1);
        jobList.add(job2);
        jobList.add(job3);
        myJobs.add(job1);
        myJobs.add(job2);
        myJobs.add(job3);
        user1.setJobList(jobList);
        user1.setMyJobs(myJobs);

        return user1;
    }
}
