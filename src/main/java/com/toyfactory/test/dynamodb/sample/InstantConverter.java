package com.toyfactory.test.dynamodb.sample;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;

import java.time.Instant;
import java.util.Date;

public class InstantConverter implements DynamoDBTypeConverter<Date, Instant> {
    @Override
    public Date convert(Instant instant) {
        return Date.from(instant);
    }

    @Override
    public Instant unconvert(Date date) {
        return date.toInstant();
    }
}
