package com.toyfactory.test.dynamodb.sample;

import com.toyfactory.test.dynamodb.repository.MemberRepository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
public class MemberService {

    private final MemberRepository memberRepository;

    public MemberService(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    public User create(String firstName, String lastName) {
        User user1 = new User();
        user1.setFirstName(firstName);
        user1.setLastName(lastName);
        user1.setBirthDt(Instant.now());

        Address address = new Address();
        address.setCity("suwon city");
        address.setZipCode(100);
        address.setStreet("wellbing-ro 70");
        user1.setAddress(address);

        return memberRepository.save(user1);
    }

    public void destroy(String id) {
        memberRepository.deleteById(id);
    }

    public boolean addJob(String id, Job job) {

        Optional<User> user = memberRepository.findById(id);

        if (user.isPresent()) {
            user.get().getJobList().add(job);
            user.get().getMyJobs().add(job);
            memberRepository.save(user.get());
            return true;
        }
        return false;
    }

    public void updateBirthDt(String id) {
        Optional<User> user = memberRepository.findById(id);

        user.ifPresent(user1 -> {
            user1.setBirthDt(Instant.now());
            memberRepository.save(user1);
        });
    }
}
