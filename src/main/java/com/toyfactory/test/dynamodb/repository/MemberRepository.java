package com.toyfactory.test.dynamodb.repository;

import com.toyfactory.test.dynamodb.sample.User;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface MemberRepository extends CrudRepository<User, String> {
    List<User> findByLastName(String lastName);
    List<User> findByFirstName(String firstName);
}
