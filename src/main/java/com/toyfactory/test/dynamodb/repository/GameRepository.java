package com.toyfactory.test.dynamodb.repository;

import com.toyfactory.test.dynamodb.singletable.domain.BaseKey;
import com.toyfactory.test.dynamodb.singletable.domain.Game;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Created by ezkiro on 2021-05-26
 */

@EnableScan
public interface GameRepository extends CrudRepository<Game, BaseKey> {

    Optional<Game> findByKey(String key);
}
