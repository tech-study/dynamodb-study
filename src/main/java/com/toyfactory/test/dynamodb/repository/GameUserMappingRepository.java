package com.toyfactory.test.dynamodb.repository;

import com.toyfactory.test.dynamodb.singletable.domain.BaseKey;
import com.toyfactory.test.dynamodb.singletable.domain.GameUserMapping;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ezkiro on 2021-05-26
 */

@EnableScan
public interface GameUserMappingRepository extends CrudRepository<GameUserMapping, BaseKey> {

}
