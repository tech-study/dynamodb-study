package com.toyfactory.test.dynamodb.repository;

import com.toyfactory.test.dynamodb.singletable.domain.BaseKey;
import com.toyfactory.test.dynamodb.singletable.domain.User2;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by ezkiro on 2021-05-26
 */

@EnableScan
public interface User2Repository extends CrudRepository<User2, BaseKey> {

    Optional<User2> findByKey(String key);
    List<User2> findByKeyContaining(String keyPattern);
}
